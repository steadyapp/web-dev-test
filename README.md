# Web App Challenge
A safe starting point for an Angular 2+ web development challenge. Comes with an expressJS backend, working AOT compilation, and some helper services and components so that you can get right in to developing the actual application. 

## Install
`npm install` will download all dependencies needed for build and dev

## Scripts
- `npm run build` - transpile the server and compile the client
- `npm run dev` - continously rebuilt and reloaded client and server
- `npm run clean` - removes the dist folder from previous builds
- `npm run test-client` - run karma for client-side tests
- `npm run test-client-dev` - run karma in watch-mode, re-running tests on save
- `npm run test-server` - run mocha tests against server
- `npm test` - run client and server tests once

## Development
Once dependencies are installed, you can begin making changes to the application in the `src` dir. If you run `npm run dev`, the application will automatically restart and refresh the browser on changes. _note: the app will not refresh on changes to the build scripts or some top-level module changes. Stop the process with `CTRL+C` and re-run `npm run dev` to pick these up_

### Backend
The ExpressJS backend lives in `src/server`. `src/server/app.ts` contains all the initialization and top level routing, as well as serving the client. Add new enpoints in the `routes` dir, and any shared services in the `services` dir. Services can be provided to all routes by initializing them in `app.ts` and passing them as part of the `APP_CONFIG`. A url `ShortenerService` along with a `LocalStorageService` are provided on the server as working demos. They are limited in functionality, but work as a simple method of generating, storing, and retrieving short urls. These do not need to be modified for the exercise, but can be if you wish to alter or extend their functionality.

### Frontend
`src/client` contains the code for the Angular front end. Most of the module, polyfill, and bundle optimization has been done, so you can focus on components, services, and routes. A few helper components like `inputgroup` and `loading-spinner` have been included as well. Use them as needed, or just as an example for other component styles. There are also some provided services for a similar purpose. 

- `BrowserStorageService` : `src/client/services/caching/browser_storage/service.ts` - A cross-browser safe wrapper for local storage operations
- `HttpCacheService` : `src/client/services/caching/http_cache/service.ts` - Handles inflight request sharing and response caching
- `ConnectionService` : `src/client/services/connection/service.ts` - Watches browser connection state to detect offline status
- `ConnectionInterceptor` : `src/client/services/connection/interceptor.ts` - Checks internet connection before making a request to avoid errors
- `MaskService` : `src/client/services/mask/service.ts` - Provides methods to mask popular input types
- `ScrollService` : `src/client/services/scroll/service.ts` - Methods for observing and interacting with the browser scroll bar
- `ShortenerService`: `src/client/services/shorten/service.ts` - Stub service for your front-end to connect to the url shortener service on the backend

### Testing
The scripts listed at the top of this file contain ways to test the client, server, or both. These tests will run all `.spec.ts` files in the `tests/` directory of `client` and `server`. A very basic test case has been included for each, but you should add tests for any new logic you add.

