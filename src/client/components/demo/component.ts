import { Component } from '@angular/core';

@Component({
    selector: 'demo-component',
    styleUrls: ['./styles.scss'],
    templateUrl: './template.html'
})
export class DemoComponent {

    isLoading = false;
}
