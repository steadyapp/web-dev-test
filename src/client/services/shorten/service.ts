import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpCacheService} from '@services/caching';
import {Observable, throwError} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ShortenService {

    constructor(
        private _http: HttpClient,
        private _cache: HttpCacheService
    ) {
    }

    /* TODO: connect to the api at /api/ and generate/retrieve short urls */

    getUrls(): Observable<{[shortUrl: string]: string}> {
        // GET /api/urls
        return throwError(new Error('not yet implemented'));
    }

    generateUrl(): Observable<{shortUrl: string, longUrl: string}> {
        // POST /api/url  -  body: {Url: string}
        return throwError(new Error('not yet implemented'));
    }
}
