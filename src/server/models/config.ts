import {LoggingService} from '../services/logger';
import {ShortenerService} from '../services/shortener';

export interface Config {
    environment?: string;
    cookie_name?: string;
    cookie_secret?: string;
    port?: number;
    log_level?: string;
    client_root?: string;
    logger?: LoggingService;
    shortener?: ShortenerService;
}
