import {Router} from 'express';
import {Config} from '../models/config';


module.exports = (APP_CONFIG: Config) => {
    const router = Router();
    const logger = APP_CONFIG.logger;
    const shortener = APP_CONFIG.shortener;

    router.post('/url', (req, res, next) => {
        const body = req.body;
        if (!body || !body.Url) {
            return res.status(400).send('Url is required');
        }
        if (!/^http(s)?:\/\/([^\s]+)\.([^\s]+)$/i.test(body.Url)) {
            return res.status(400).send('Url must be a fully qualified Url');
        }
        shortener.generateShortUrl(body.Url)
        .subscribe(
            shortUrl => res.send({
                shortUrl,
                longUrl: body.Url
            }),
            err => res.status(500).send(err)
        );
    });

    router.get('/urls', (req, res, next) => {
        return shortener.getAllUrls()
        .subscribe(
            urlMap => res.send(urlMap),
            err => res.status(500).send(err)
        );
    });

    // Return middleware router
    return router;
}
