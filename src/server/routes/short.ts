import {Router} from 'express';
import {Config} from '../models/config';


module.exports = (APP_CONFIG: Config) => {
    const router = Router();
    const logger = APP_CONFIG.logger;
    const shortener = APP_CONFIG.shortener;

    router.use((req, res, next) => {
        const shortPath = req.url.slice(1);
        shortener.getFullUrl(shortPath)
        .subscribe(fullUrl => {
            return res.redirect(307, fullUrl);
        }, err => {
            logger.logError(err);
            return res.status(404).send('Url not found');
        });
    });

    // Return middleware router
    return router;
}
