import {Observable} from 'rxjs';

export interface StorageService {

    setUrl(longUrl: string, shortUrl: string): Observable<any>;

    getUrl(shortUrl: string): Observable<string>;

    getAll(): Observable<{[key: string]: string}>;
}
