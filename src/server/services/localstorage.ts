import {readFileSync, writeFileSync} from 'fs';
import {Observable, of, throwError} from 'rxjs';
import {StorageService} from './interfaces/storage';

export class LocalStorageService implements StorageService {

    private _storage: {[key: string]: string} = {};

    constructor(
        private _storageFile: string = './data.json'
    ) {
        try {
            this._storage = JSON.parse(readFileSync(this._storageFile).toString());
        } catch (e) {
            console.warn('Could not parse data file. May be malformed, or this may the first run');
        }
    }

    getAll(): Observable<{[key: string]: string}> {
        return of(this._storage);
    }

    getUrl(shortUrl: string): Observable<string> {
        if (shortUrl in this._storage) {
            return of(this._storage[shortUrl]);
        } else {
            return throwError(new Error(`Url not found`));
        }
    }

    setUrl(longUrl: string, shortUrl: string): Observable<any> {
        this._storage[shortUrl] = longUrl;
        try {
            writeFileSync(this._storageFile, JSON.stringify(this._storage));
            return of(null);
        } catch (e) {
            return throwError(e);
        }
    }
}
