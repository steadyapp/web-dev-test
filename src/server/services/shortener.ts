import {createHash} from 'crypto';
import {Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {StorageService} from './interfaces/storage';

export class ShortenerService {

    // exists for recently created urls, within the same server session
    private _memoryCache: {[key: string]: string} = {};

    constructor(
        private _storage: StorageService
    ) {
    }

    getAllUrls(): Observable<{[key: string]: string}> {
        return this._storage.getAll();
    }

    generateShortUrl(longUrl: string): Observable<string> {
        const short = createHash('sha1')
        .update(longUrl).digest('base64')
        .replace(/\//g, '-')
        .replace(/=/g, '');
        return this._storage.setUrl(longUrl, short)
        .pipe(
            map(_ => short),
            tap(_ => this._memoryCache[short] = longUrl)
        );
    }

    getFullUrl(shortUrl: string): Observable<string> {
        if (shortUrl in this._memoryCache) {
            return of(this._memoryCache[shortUrl]);
        } else {
            return this._storage.getUrl(shortUrl)
            .pipe(
                tap(longurl => this._memoryCache[shortUrl] = longurl)
            );
        }
    }
}
